package com.zhipeipt.pdf;

import org.junit.Test;

public class TestRender {

    private static final String output = System.getProperty("user.dir") + "\\test.pdf";
    private static final String font = System.getProperty("user.dir") + "\\msyh.ttf";


    @Test
    public void start() throws Exception {

        PdfSetup setup = new PdfSetup(output, font);
        setup.setDirectory(true);
        setup.setCover(true);
        setup.setFooter(true);
        setup.setHeader(true);
        Itext7Pdf.getPdf(new TestPdfRender(), setup).write();
    }
}
