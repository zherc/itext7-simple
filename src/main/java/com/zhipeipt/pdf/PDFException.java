package com.zhipeipt.pdf;

public class PDFException extends RuntimeException{
    public PDFException(String message) {
        super(message);
    }

    public PDFException(String message, Throwable cause) {
        super(message, cause);
    }
}
