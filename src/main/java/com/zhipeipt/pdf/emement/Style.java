package com.zhipeipt.pdf.emement;

import com.zhipeipt.pdf.emement.Margin;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Style {
    private Margin margin = new Margin();

    public Style() {
    }

    public Style margin(String margin) {
        this.margin = Margin.parse(margin);
        return this;
    }

    public Style margin(Margin margin) {
        this.margin = margin;
        return this;
    }
}
