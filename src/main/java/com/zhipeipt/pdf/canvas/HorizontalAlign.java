package com.zhipeipt.pdf.canvas;

public enum HorizontalAlign {
    LEFT, CENTER, RIGHT
}
