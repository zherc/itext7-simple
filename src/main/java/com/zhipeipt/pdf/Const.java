package com.zhipeipt.pdf;

public interface Const {
    String COVER = "cover";
    String DIRECTORY = "directory";

    String DEFAULT_FONT = "defaultFont";
}
